const express = require(`express`);
const app = express();
const { morganMiddleWare } = require(`./../../logger/morgan`);
const { sequelize } = require(`./../config/database_psql`);
const { directorRouter, moviesRouter } = require(`./../routers/router`);
const bodyParser = require(`body-parser`);
const port = process.env.PORT;

sequelize
    .authenticate()
    .then(() => console.log(`Postgres database connected..`))
    .catch(() => console.log(`Database creditial wrong!!`));

sequelize.sync();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(morganMiddleWare);

app.use(`/api/director`, directorRouter);

app.use(`/api/movies`, moviesRouter);

app.use((err, req, res, next) => {
    console.error(err.stack);
    res.status(500).send("Something broke!");
});

app.listen(port, () => console.log(`Server connected on port: ${port}`));