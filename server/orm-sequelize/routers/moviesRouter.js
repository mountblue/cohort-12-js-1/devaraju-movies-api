const express = require(`express`);
const router = express.Router();
const moviesArr = require(`./../../../data/movies`);
const { Movies, Director } = require(`./../models/model`);
const schema = require(`./../../validation/mValidation`);

router.get(`/all`, async(req, res, next) => {
    try {
        const result = await Movies.findAll();
        if (result.length === 0) {
            console.log(`Error in data!!`);
            res.status(404).send(`Error in data!!`);
        } else {
            console.log(result);
            res.status(200).send(result);
        }
    } catch (error) {
        console.error(error.message);
        res.status(404).send(error.message);
    }
});

router.get(`/:id`, async(req, res, next) => {
    try {
        await schema.validateAsync({ ID: req.params.id });
        const result = await Movies.findAll({ where: { id: req.params.id } });
        if (result.length === 0) {
            console.log(`Error in ID!!`);
            res.status(404).send(`Error in ID!!`);
        } else {
            console.log(result.dataValues);
            res.status(200).send(result);
        }
    } catch (error) {
        console.error(error.message);
        res.status(405).send(error.message);
    }
});

router.post(`/insertAll`, async(req, res, next) => {
    try {
        await schema.validateAsync({
            RANK: req.body.rank,
            TITLE: req.body.title,
            DESCRIPTION: req.body.description,
            RUNTIME: req.body.runtime,
            GENRE: req.body.genre,
            RATING: req.body.rating,
            METASCORE: req.body.metascore,
            VOTES: req.body.votes,
            GROSS: req.body.gross_earning_in_mil,
            DIRECTORID: req.body.directorid,
            ACTOR: req.body.actor,
            YEAR: req.body.year
        });
        const result = await Movies.create({
            rank: req.body.rank,
            title: req.body.title,
            description: req.body.description,
            runtime: req.body.runtime,
            genre: req.body.genre,
            rating: req.body.rating,
            metascore: req.body.metascore,
            votes: req.body.votes,
            gross_earning_in_mil: req.body.gross_earning_in_mil,
            directorid: req.body.directorid,
            actor: req.body.actor,
            year: req.body.year
        });
        console.log(result.dataValues);
        console.log(`Inserted successfully..`);
        res.status(201).send(`Inserted successfully..`);
    } catch (error) {
        console.error(error.message);
        res.status(405).send(error.message);
    }
});

router.post(`/insert`, async(req, res, next) => {
    try {
        await schema.validateAsync({
            RANK: req.body.rank,
            TITLE: req.body.title,
            DESCRIPTION: req.body.description,
            RUNTIME: req.body.runtime,
            GENRE: req.body.genre,
            RATING: req.body.rating,
            METASCORE: req.body.metascore,
            VOTES: req.body.votes,
            GROSS: req.body.gross_earning_in_mil,
            DIRECTORID: req.body.directorid,
            ACTOR: req.body.actor,
            YEAR: req.body.year
        });
        const result = await Movies.create({
            rank: req.body.rank,
            title: req.body.title,
            description: req.body.description,
            runtime: req.body.runtime,
            genre: req.body.genre,
            rating: req.body.rating,
            metascore: req.body.metascore,
            votes: req.body.votes,
            gross_earning_in_mil: req.body.gross_earning_in_mil,
            directorid: req.body.directorid,
            actor: req.body.actor,
            year: req.body.year
        });
        console.log(result.dataValues);
        console.log(`Inserted successfully..`);
        res.status(201).send(`Inserted successfully..`);
    } catch (error) {
        console.error(error.message);
        res.status(405).send(error.message);
    }
});

router.put(`/update/:rank`, async(req, res, next) => {
    try {
        await schema.validateAsync({
            RANK: req.body.rank,
            TITLE: req.body.title,
            DESCRIPTION: req.body.description,
            RUNTIME: req.body.runtime,
            GENRE: req.body.genre,
            RATING: req.body.rating,
            METASCORE: req.body.metascore,
            VOTES: req.body.votes,
            GROSS: req.body.gross_earning_in_mil,
            ACTOR: req.body.actor,
            YEAR: req.body.year
        });
        const result = await Movies.update({
            title: req.body.title,
            description: req.body.description,
            runtime: req.body.runtime,
            genre: req.body.genre,
            rating: req.body.rating,
            metascore: req.body.metascore,
            votes: req.body.votes,
            gross_earning_in_mil: req.body.gross_earning_in_mil,
            actor: req.body.actor,
            year: req.body.year
        }, {
            where: { rank: req.params.rank }
        });
        if (result.length === 0) {
            console.log(`Error in ID!!`);
            res.status(404).send(`Error in ID!!`);
        } else {
            console.log(`${result} Row Updated successfully..`);
            res.status(201).send(`${result} Row Updated successfully..`);
        }
    } catch (error) {
        console.error(error.message);
        res.status(400).send(error.message);
    }
});

router.delete(`/delete/:rank`, async(req, res, next) => {
    try {
        await schema.validateAsync({ RANK: req.params.rank });
        const result = await Movies.destroy({ where: { rank: req.params.rank } });
        if (result.length === 0) {
            console.log(`Error in ID!!`);
            res.status(404).send(`Error in ID!!`);
        } else {
            console.log(`${result} Row Deleted successfully..`);
            res.status(410).send(`${result} Row Deleted successfully..`);
        }
    } catch (error) {
        console.error(error.message);
        res.status(405).send(`Error in record!!`);
    }
});

module.exports.moviesRouter = router;