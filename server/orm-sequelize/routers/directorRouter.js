const express = require(`express`);
const router = express.Router();
const { Director } = require(`./../models/model`);
const moviesArr = require(`./../../../data/movies`);
const schema = require(`./../../validation/dValidation`);

router.get(`/all`, async(req, res, next) => {
    try {
        const result = await Director.findAll();
        if (result.length === 0) {
            console.log(`Error in data!!`);
            res.status(404).send(`Error in data!!`);
        } else {
            console.log(result);
            res.status(200).send(result);
        }
    } catch (error) {
        console.error(error.message);
        res.status(404).send(`Error in data!!`);
    }
});

router.get(`/:id`, async(req, res, next) => {
    try {
        await schema.validateAsync({ ID: req.params.id });
        const result = await Director.findAll({ where: { id: req.params.id } });
        if (result.length === 0) {
            console.log(`Error in ID!!`);
            res.status(404).send(`Error in ID!!`);
        } else {
            console.log(result[0].dataValues);
            res.status(200).send(result);
        }
    } catch (error) {
        console.error(error.message);
        res.status(405).send(`Error in ID!!`);
    }
});

router.post(`/insertAll`, async(req, res) => {
    try {
        await schema.validateAsync({ ID: req.body.id, DNAME: req.body.dname });
        const directorArr = moviesArr.reduce((directorArr, movies) => {
            let directorTable = {};
            directorTable.id = `DEFAULT`;
            directorTable.dname = movies.Director;
            directorArr.push(directorTable);
            return directorArr;
        }, []);
        directorJsonObj = directorArr.map(JSON.stringify);
        directorSet = new Set(directorJsonObj);
        directorData = Array.from(directorSet).map(JSON.parse);

        Director.bulkCreate(directorData, {
            returning: true
        });
        res.status(200).send(`Director data inserted successfully..`);
        console.log(`Director data inserted successfully..`);
    } catch (error) {
        console.error(error.message);
        res.status(404).send(error.message);
    }
});

router.post(`/insert`, async(req, res, next) => {
    try {
        await schema.validateAsync({ ID: req.body.id, DNAME: req.body.dname });
        const result = await Director.create({ dname: req.body.dname });
        console.log(result.dataValues);
        console.log(`Inserted successfully..`);
        res.status(201).send(`Inserted successfully..`);
    } catch (error) {
        console.error(error.message);
        res.status(405).send(error.message);
    }
});

router.put(`/update/:id`, async(req, res, next) => {
    try {
        await schema.validateAsync({ ID: req.params.id });
        const result = await Director.update({
            dname: req.body.dname
        }, {
            where: {
                id: req.params.id
            }
        });
        if (result.length === 0) {
            console.log(`Error in ID!!`);
            res.status(404).send(`Error in ID!!`);
        } else {
            console.log(`${result} Row Updated successfully..`);
            res.status(201).send(`${result} Row Updated successfully..`);
        }
    } catch (error) {
        console.error(error.message);
        res.status(400).send(error.message);
    }
});

router.delete(`/delete/:id`, async(req, res, next) => {
    try {
        await schema.validateAsync({ ID: req.params.id });
        const result = await Director.destroy({ where: { id: req.params.id } });
        if (result.length === 0) {
            console.log(`Error in ID!!`);
            res.status(404).send(`Error in ID!!`);
        } else {
            console.log(`${result} Row Deleted successfully..`);
            res.status(410).send(`${result} Row Deleted successfully..`);
        }
    } catch (error) {
        console.error(error.message);
        res.status(405).send(error.message);
    }
});

module.exports.directorRouter = router;