const { directorRouter } = require(`./directorRouter`);
const { moviesRouter } = require(`./moviesRouter`);

module.exports = {
    directorRouter,
    moviesRouter
};