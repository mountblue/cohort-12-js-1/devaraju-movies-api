const moviesArr = require(`./../../data/movies`);
const { Pool } = require(`pg`);
const fetch = require(`node-fetch`);

// const pool = new Pool({
//     user: process.env.PGUSER,
//     password: process.env.PGPASSWORD,
//     database: process.env.PGDATABASE,
//     port: process.env.PGPORT,
//     host: process.env.PGHOST
// });

// const pool = new Pool({
//     user: "postgres",
//     password: "abcd",
//     database: "orm",
//     port: 5432,
//     host: "localhost"
// });

// const createDirectorTable = `CREATE TABLE IF NOT EXISTS director_models(
//         id SERIAL PRIMARY KEY,
//         dname VARCHAR(100) UNIQUE);`;

// const createMovieTable = `CREATE TABLE IF NOT EXISTS movies_models(
//         rank INT,
//         title VARCHAR(100),
//         description VARCHAR(500),
//         runtime INT,
//         genre VARCHAR(50),
//         rating FLOAT,
//         metascore VARCHAR(20),
//         votes BIGINT,
//         gross_earning_In_mil VARCHAR(20),
//         directorid INT NOT NULL REFERENCES director_models ON DELETE CASCADE ON UPDATE CASCADE,
//         actor VARCHAR(100),
//         year INT,
//         FOREIGN KEY(directorid) REFERENCES director_models(id));`;

// pool.connect().then(client => {
//     client.query(createDirectorTable).then(() => {
//         console.log(`Director table created!!`);
//     });
//     client.query(createMovieTable).then(() => {
//         console.log(`Movies table created!!`);
//     });
//     // Insert data into director table
//     moviesArr.forEach(director => {
//         client
//             .query(`INSERT INTO director_models(dname) VALUES('${director.Director}')`)
//             .catch(error => console.error(error.message));
//     });
//     client.query(`SELECT NOW()`).then(() => {
//         client.release();
//         console.log(`Director Data Inserted successfully!!`);
//     });
// });

// Insert data into movies table
// fetch(
//         `https://trello-attachments.s3.amazonaws.com/5e50bb88999f9e4da142aa14/5e65dcaf4fd2bb2d2f1612fd/x/f91e9aa5fed69a5c52405ddd2cf1c003/movies.json`
//     )
//     .then(result => result.json())
//     .then(data => {
//         let movie = [];
//         data.map(prop => {
//             movie.push(Object.values(prop));
//             return prop;
//         });
//         return movie;
//     })
//     .then(data => {
//         let id = [];
//         data.forEach(row => {
//             pool.connect().then(client => {
//                 client
//                     .query(
//                         `SELECT id
//                                   FROM director_models
//                                   WHERE dname = '${row[9]}'`
//                     )
//                     .then(res => {
//                         id.push(res.rows[0].id);
//                         row.splice(9, 1, id.shift());
//                         return row;
//                     })
//                     .then(row => {
//                         client.query(
//                             `INSERT INTO movies_models VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12)`,
//                             row
//                         );
//                         client.release();
//                     });
//             });
//         });
//     });

const id = async directorName => {
    try {
        let result = await Director.findAll({
            where: {
                dname: directorName
            }
        });
        return result[0].dataValues.id;
    } catch (error) {
        console.error(error.message);
    }
};

const arr = moviesArr;

const res = moviesArr.map(async i => {
    moviesArr.Director = await id(i.Director);
}, []);

// const res = moviesArr.reduce((arr, data) => {
//     const result = async() => await id(data.Director);
//     console.log(result);

//     let obj = {};
//     obj.rank = data.Rank;
//     obj.title = data.Title;
//     obj.description = data.Description;
//     obj.runtime = data.Runtime;
//     obj.genre = data.Genre;
//     obj.rating = data.Rating;
//     obj.metascore = data.Metascore;
//     obj.votes = data.Votes;
//     obj.gross_earning_in_mil = data.Gross_Earning_in_Mil;
//     obj.directorid = result;
//     obj.actor = data.Actor;
//     obj.year = data.Year;
//     arr.push(obj);
//     return arr;
// }, []);

console.log(arr);

// movies index.js
// const express = require(`express`);
// const router = express();
// const bodyParser = require(`body-parser`);
// const db = require(`./query`);

// router.use(bodyParser.json());
// router.use(bodyParser.urlencoded({ extended: true }));

// router.get(`/api/movies`, db.getMovies);
// router.get(`/api/movies/:directorid`, db.getMoviesId);
// router.post(`/api/movies/insert`, db.setMoviesById);
// router.put(`/api/movies/update`, db.updateMovies);
// router.delete(`/api/movies/deleteMovie/:rank`, db.deleteMovies);
// router.delete(`/api/movies/deleteKey`, db.dropForeignKey);
// router.post(`/api/movies/cascade`, db.updateCascade);

// router.listen(2000, () => console.log(`Server connected at port: 2000`));

// // movies query
// const { Pool } = require(`pg`);

// const pool = new Pool({
//     user: process.env.PGUSER,
//     password: process.env.PGPASSWORD,
//     database: process.env.PGDATABASE,
//     port: process.env.PGPORT,
//     host: process.env.PGHOST
// });

// const getMovies = (req, res) => {
//     pool
//         .query(`SELECT * FROM movies`)
//         .then(result => {
//             console.table(result.rows);
//             res.status(200).send(result.rows);
//         })
//         .catch(error => {
//             console.error(error.message);
//             res.status(404).send(`Table Not Found!!`);
//         });
// };

// const getMoviesId = (req, res) => {
//     pool
//         .query(`SELECT * FROM movies WHERE directorid = ${req.params.directorid}`)
//         .then(result => {
//             console.log(result.rows);
//             res.status(200).send(result.rows);
//         })
//         .catch(error => {
//             console.error(error.message);
//             res.status(404).send(`ID Not Available!!`);
//         });
// };

// const setMoviesById = (req, res) => {
//     pool
//         .query(`SELECT id FROM director WHERE dname = '${req.body.directorid}'`)
//         .then(directorId => {
//             pool.query(
//                 `INSERT INTO movies VALUES(${req.body.rank},'${req.body.title}','${req.body.description}',${req.body.runtime},'${req.body.genre}',${req.body.rating},'${req.body.metascore}','${req.body.votes}','${req.body.gross_earning_in_mil}',${directorId.rows[0].id},'${req.body.actor}',${req.body.year})`
//             );
//         })
//         .then(() => {
//             console.log(`Inserted successfully`);
//             res.status(201).send(`Inserted successfully`);
//         })
//         .catch(error => {
//             console.error(error.message);
//             res.status(404).send(`ID Not Available!!`);
//         });
// };

// const updateMovies = (req, res) => {
//     pool
//         .query(
//             `UPDATE movies SET rank=${req.body.rank},description='${req.body.description}',runtime=${req.body.runtime},genre='${req.body.genre}',rating=${req.body.rating},metascore='${req.body.metascore}',votes='${req.body.votes}',gross_earning_in_mil='${req.body.gross_earning_in_mil}',actor='${req.body.actor}',year=${req.body.year} WHERE title='${req.body.title}'`
//         )
//         .then(() => {
//             console.log(`Updated succesfully`);
//             res.status(200).send(`Updated succesfully`);
//         })
//         .catch(error => {
//             console.log(error.message);
//             res.status(404).send(`Title Not Available!!`);
//         });
// };

// const deleteMovies = (req, res) => {
//     pool
//         .query(`DELETE FROM movies WHERE rank = ${req.params.rank}`)
//         .then(() => {
//             console.log(`Deleted succesfully`);
//             res.status(200).send(`Deleted succesfully`);
//         })
//         .catch(error => {
//             console.log(error.message);
//             res.status(404).send(`ID Not Available!!`);
//         });
// };

// const dropForeignKey = (req, res) => {
//     pool
//         .query(`ALTER TABLE movies DROP CONSTRAINT "directorid__fkey"`)
//         .then(() => {
//             console.log(`Foriegn Key Dropped`);
//             res.status(201).send(`Foriegn Key Dropped`);
//         })
//         .catch(error => {
//             console.error(error.message);
//             res.status(404).send(`Table Not Found!!`);
//         });
// };

// const updateCascade = (req, res) => {
//     pool
//         .query(
//             `ALTER TABLE movies ADD CONSTRAINT directorid_fkey FOREIGN KEY (directorid) REFERENCES director(id) ON UPDATE CASCADE ON DELETE RESTRICT`
//         )
//         .then(() => {
//             console.log(`Cascade Updated`);
//             res.status(201).send(`Cascade Updated`);
//         })
//         .catch(error => {
//             console.error(error.message);
//             res.status(404).send(`Table Not Found!!`);
//         });
// };

// module.exports = {
//     getMovies,
//     getMoviesId,
//     setMoviesById,
//     updateMovies,
//     deleteMovies,
//     dropForeignKey,
//     updateCascade
// };