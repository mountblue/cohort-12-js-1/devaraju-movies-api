const Sequelize = require(`sequelize`);
const { sequelize } = require(`./../config/database_psql`);

class Director extends Sequelize.Model {}

Director.init({
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    dname: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false
    }
}, {
    underscored: true,
    sequelize,
    modelName: `directorModel`
});

module.exports.Director = Director;