const { Director } = require(`./directorModel`);
const { Movies } = require(`./moviesModel`);

module.exports = {
    Director,
    Movies
};