const Sequelize = require(`sequelize`);
const { sequelize } = require(`./../config/database_psql`);
const { Director } = require(`./directorModel`);

class Movies extends Sequelize.Model {}

Movies.init({
    rank: {
        type: Sequelize.INTEGER,
        allowNull: false,
        primaryKey: true
    },
    title: {
        type: Sequelize.STRING,
        allowNull: false
    },
    description: {
        type: Sequelize.STRING,
        allowNull: true
    },
    runtime: {
        type: Sequelize.INTEGER,
        allowNull: true
    },
    genre: {
        type: Sequelize.STRING,
        allowNull: true
    },
    rating: {
        type: Sequelize.FLOAT,
        allowNull: true
    },
    metascore: {
        type: Sequelize.STRING,
        allowNull: true
    },
    votes: {
        type: Sequelize.BIGINT,
        allowNull: true
    },
    gross_Earning_in_Mil: {
        type: Sequelize.STRING,
        allowNull: true
    },
    directorid: {
        type: Sequelize.INTEGER,
        allowNull: false,
        foreignKey: true
    },
    actor: {
        type: Sequelize.STRING,
        allowNull: true
    },
    year: {
        type: Sequelize.INTEGER,
        allowNull: true
    }
}, {
    underscored: true,
    sequelize,
    modelName: `moviesModel`
});

Director.hasMany(Movies, { foreignKey: `directorid` });

module.exports.Movies = Movies;