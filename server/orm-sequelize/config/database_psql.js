const Sequelize = require(`sequelize`);

const sequelize = new Sequelize(
    process.env.PGDATABASE,
    process.env.PGUSER,
    process.env.PGPASSWORD, {
        host: process.env.PGHOST,
        dialect: process.env.PGUSER,
        pool: {
            min: 0,
            max: 100,
            idle: 1000
        }
    }
);

module.exports.sequelize = sequelize;