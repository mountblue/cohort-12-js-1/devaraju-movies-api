const morgan = require(`morgan`);
const chalk = require(`chalk`);

const morganMiddleWare = morgan((tokens, req, res) => {
    return [
        chalk.green.bold(tokens.method(req, res)),
        chalk.magenta(tokens.status(req, res)),
        chalk.cyanBright(tokens.url(req, res)),
        chalk.redBright(tokens[`response-time`](req, res)),
        chalk.redBright(`ms`)
    ].join(` `);
});

module.exports = {
    morganMiddleWare
};