const express = require(`express`);
const app = express();
const { directorRouter, moviesRouter } = require(`./../routers/router`);
const bodyParser = require(`body-parser`);
const { morganMiddleWare } = require(`./../logger/morgan.js`);
const { loggerMiddleWare } = require(`./../logger/winston.js`);
const port = process.env.PORT;

app.use(morganMiddleWare);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(`/api/director`, directorRouter);

app.use(`/api/movies`, moviesRouter);

app.use((err, req, res, next) => {
    console.error(err.stack);
    res.status(500).send("Something broke!");
});

app.listen(port, () => console.log(`Server connected on port: ${port}`));