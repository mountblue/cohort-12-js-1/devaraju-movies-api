const {
    getDirector,
    getDirectorById,
    setDirector,
    updateDirector,
    deleteDirector
} = require(`./directorModel`);

const {
    getMovies,
    getMoviesId,
    setMoviesById,
    updateMovies,
    deleteMovies
} = require(`./moviesModel`);

module.exports = {
    getDirector,
    getDirectorById,
    setDirector,
    updateDirector,
    deleteDirector,
    getMovies,
    getMoviesId,
    setMoviesById,
    updateMovies,
    deleteMovies
};