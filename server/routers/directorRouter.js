const express = require(`express`);
const router = express.Router();
const schema = require(`../validation/dValidation.js`);

const {
    getDirector,
    getDirectorById,
    setDirector,
    updateDirector,
    deleteDirector
} = require(`../models/model.js`);

router.get(`/all`, async(req, res, next) => {
    try {
        const result = await getDirector();
        if (result.rows.length === 0) {
            console.log(`Error in data!!`);
            res.status(404).send(`Error in data!!`);
        } else {
            console.table(result.rows);
            res.status(200).send(result.rows);
        }
    } catch (error) {
        console.error(error.message);
        res.status(404).send(error.message);
    }
});

router.get(`/:id`, async(req, res) => {
    try {
        await schema.validateAsync({ ID: req.params.id });
        const result = await getDirectorById(req.params.id);
        if (result.rows.length === 0) {
            console.log(`Error in ID!!`);
            res.status(404).send(`Error in ID!!`);
        } else {
            console.table(result.rows);
            res.status(200).send(result.rows);
        }
    } catch (error) {
        console.error(error.message);
        res.status(400).send(error.message);
    }
});

router.post(`/insert`, async(req, res) => {
    try {
        await schema.validateAsync({ ID: req.body.id, DNAME: req.body.dname });
        const result = await setDirector(req.body.id, req.body.dname);
        console.log(`${result.rowCount} Row Inserted successfully`);
        res.status(201).send(`${result.rowCount} Row Inserted successfully`);
    } catch (error) {
        console.error(error.message);
        res.status(405).send(error.message);
    }
});

router.put(`/update/:id`, async(req, res) => {
    try {
        await schema.validateAsync({ ID: req.body.id, DNAME: req.body.dname });
        const result = await updateDirector(req.params.id, req.body.dname);
        if (result.rowCount === 0) {
            console.log(`Error in ID!!`);
            res.status(404).send(`Error in ID!!`);
        } else {
            console.log(`${result.rowCount} Row Updated successfully`);
            res.status(200).send(`${result.rowCount} Row Updated successfully`);
        }
    } catch (error) {
        console.error(error.message);
        res.status(405).send(error.message);
    }
});

router.delete(`/delete/:id`, async(req, res) => {
    try {
        await schema.validateAsync({ ID: req.params.id });
        const result = await deleteDirector(req.params.id);
        if (result.rowCount === 0) {
            console.log(`Error in ID!!`);
            res.status(404).send(`Error in ID!!`);
        } else {
            console.log(`${result.rowCount} Row Deleted successfully`);
            res.status(410).send(`${result.rowCount} Row Deleted successfully`);
        }
    } catch (error) {
        console.error(error.message);
        res.status(405).send(error.message);
    }
});

module.exports.directorRouter = router;