const joi = require("@hapi/joi");

const schema = joi.object({
    ID: joi.number().integer(),
    DNAME: joi.string()
});

module.exports = schema;